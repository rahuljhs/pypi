from hello_world_lib import hello_world


def test_hello_world():
    assert hello_world() == "Hello, World!"
    assert hello_world("Alice") == "Hello, Alice!"
